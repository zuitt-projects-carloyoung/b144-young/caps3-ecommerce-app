import React, {useState, useEffect} from "react";
import Layout from './Layout'
import{getProducts} from './apiCore'
import Card from './Card' 
import Search from './Search'

const Home = () => {
	const [productsBySell, setproductsBySell] = useState([])
	const [productsByArrival, setproductsByArrival] = useState([])
	const [error, setError] = useState(false)

	
	const loadProductsBySell = () => {
		getProducts('sold').then(data => {
			if(data.error) {
				setError(data.error)
			} else {
				setproductsBySell(data)
			}
		})
	}

	const loadProductsByArrival = () => {
		getProducts('createdAt').then(data => {
			if(data.error) {
				setError(data.error)
			} else {
				setproductsByArrival(data)
			}
		})
	}

	useEffect(() => {
		loadProductsByArrival()
		loadProductsBySell()
	}, [])

	return (
		<Layout title="Gelato Express" description="It's all about artisanal" className="container-fluid"  id="landing">
		<Search />
		<h2 className="mb-4 text-white">Best Sellers</h2>
		<div className ="row">
			
			{productsBySell.map((product, i) => (
			<div key={i} className="col-4 mb-3">
			<Card  product={product} />
			</div>
			))}

		</div>
		
		<h2 className="mb-4 text-white">New Arrivals</h2>
		<div className ="row">
		{productsByArrival.map((product, i) => (
			<div key={i} className="col-4 mb-3">
			<Card  product={product} />
			</div>
			))}
		</div>
		
		</Layout>

		)
}

export default Home;
import React from 'react'
import {API} from '../config'

const ShowImage = ({ item, url }) => (
	<div className="product-img">
		<img
		 src={`http://localhost:8000/api/${url}/photo/${item._id}`}
		 alt={item.name}
		 class="img-fluid col-md-6 mx-auto my-2"
		 id="image"
		 style={{maxHeight:"100%", maxWidth: "100%"}}
		 />
	</div>
)

export default ShowImage;